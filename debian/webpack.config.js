'use strict';
var path = require('path');
var config = {
  target: 'web',
  resolve: {
    modules: ['/usr/lib/nodejs', '.'],
  },
  resolveLoader: {
    modules: ['/usr/lib/nodejs'],
  },
  output: {
    libraryTarget: 'umd'
  },
  module: { rules: [ { use: [ 'babel-loader' ] } ] }
}
module.exports = config;
